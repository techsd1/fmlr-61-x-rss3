/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2020 Miromico AG
 * All rights reserved.
 */

#include "lora_thread.h"
#include "bsp_pin_cfg.h"

#include "euis.h"
#include "common.h"

#include "miromico/log.h"
#include "miromico/button.h"
#include "miromico/sht21.h"
#include "miromico/led.h"

#include <stdio.h>

/* Various helper functions and static variable definitions
 *
 */

#define MAX_PAYLOAD_SIZE (51)

extern const sf_lora_keys_t keys;

static volatile bool _isJoined = false;
static volatile bool _isDoneTX = false;

static SHT_HandleTypedef _sht21;

static void _LED_init() {
    setRGB(false, false, false);
}

static void _sensor_init() {
    ssp_err_t err = SSP_SUCCESS;
    err = g_i2c0.p_api->open(g_i2c0.p_ctrl, g_i2c0.p_cfg);
    check_success(err);
    _sht21.hi2c = (void*)&g_i2c0;
    SHTInitSensor(&_sht21);
}

static void _lora_init() {
    ssp_err_t err = SSP_SUCCESS;
    err = g_sf_lora0.p_api->open(g_sf_lora0.p_ctrl, g_sf_lora0.p_cfg);
    check_success(err);
    err = g_sf_lora0.p_api->set_log_func(g_sf_lora0.p_ctrl, log_lora);
    check_success(err);
    err = g_sf_lora0.p_api->set_keys(g_sf_lora0.p_ctrl, (sf_lora_keys_t* const)&keys);
    check_success(err);
}

static void _lora_join() {
    ssp_err_t err = SSP_SUCCESS;
    err = g_sf_lora0.p_api->join(g_sf_lora0.p_ctrl);
    check_success(err);
}

/* LoRa Thread entry function
 * Add your lora-specific application code here
 * Add additional threads with independent entry points using the Synergy Configurator
 * */

void lora_thread_entry(void)
{
    ssp_err_t err = SSP_SUCCESS;

    print_logo();

    ssp_version_t ssp_lora_version;
    const char * ssp_lmic_version;
    err = g_sf_lora0.p_api->version(&ssp_lora_version, &ssp_lmic_version);

    log_app("[[ FMLR-61-X-RSS3 Development Board Example Firmware ]]");
    log_app("Based on %s", ssp_lmic_version);
#ifdef DEBUG
    log_app("Warning: This is a DEBUG build");
#endif
    log_app("");
    log_app(".pack API  version %u.%u",  (uint8_t)ssp_lora_version.api_version_major,  (uint8_t)ssp_lora_version.api_version_minor);
    log_app(".pack code version %u.%u", (uint8_t)ssp_lora_version.code_version_major, (uint8_t)ssp_lora_version.code_version_minor);
    log_app("");
    log_app("Wait for join and push the button to transmit sensor values");
    log_app("");

    _LED_init();
    _sensor_init();

    _lora_init();
    _lora_join();

    while (true)
    {
        bool joined = _isJoined;
        bool button_pressed = getButton();

        if (joined) {
            if (button_pressed && _isDoneTX) {
                log_app("Button pressed, sending LoRaWAN packet");

                // Prepare a LoRaWAN message
                uint8_t payload[MAX_PAYLOAD_SIZE];
                int16_t value = 0;
                SHTGetTemp(&_sht21, &value);
                // This could as well be raw binary data, instead of text
                snprintf((char *)payload, MAX_PAYLOAD_SIZE, "TEMP:%04d", value);
                size_t length = strlen((char*)payload);

                log_app("Sending <%s> (%u bytes)", payload, length);

                sf_lora_app_data_t data = {
                   .buff = payload, .buff_size = 0, .port = 2
                };
                data.buff_size = (uint8_t)(length);

                err = g_sf_lora0.p_api->send(g_sf_lora0.p_ctrl, &data, true);
                check_success(err);
            }
        }

        // indicate what's going on
        setRGB(false, !_isJoined, _isJoined && !_isDoneTX);

        tx_thread_sleep(1);
    }
}


static void _print_event (sf_lora_event_t event) {
    static const char* evnames[] = {
        [EVENT_LORA_SCAN_TIMEOUT]   = "SCAN_TIMEOUT",
        [EVENT_LORA_BEACON_FOUND]   = "BEACON_FOUND",
        [EVENT_LORA_BEACON_MISSED]  = "BEACON_MISSED",
        [EVENT_LORA_BEACON_TRACKED] = "BEACON_TRACKED",
        [EVENT_LORA_JOINING]        = "JOINING",
        [EVENT_LORA_JOINED]         = "JOINED",
        [EVENT_LORA_RFU1]           = "RFU1",
        [EVENT_LORA_JOIN_FAILED]    = "JOIN_FAILED",
        [EVENT_LORA_REJOIN_FAILED]  = "REJOIN_FAILED",
        [EVENT_LORA_TXCOMPLETE]     = "TXCOMPLETE",
        [EVENT_LORA_LOST_TSYNC]     = "LOST_TSYNC",
        [EVENT_LORA_RESET]          = "RESET",
        [EVENT_LORA_RXCOMPLETE]     = "RXCOMPLETE",
        [EVENT_LORA_LINK_DEAD]      = "LINK_DEAD",
        [EVENT_LORA_LINK_ALIVE]     = "LINK_ALIVE",
        [EVENT_LORA_SCAN_FOUND]     = "SCAN_FOUND",
        [EVENT_LORA_TXSTART]        = "TXSTART",
        [EVENT_LORA_TXDONE]         = "TXDONE",
        [EVENT_LORA_DATARATE]       = "DATARATE",
        [EVENT_LORA_START_SCAN]     = "START_SCAN",
        [EVENT_LORA_ADR_BACKOFF]    = "ADR_BACKOFF",
    };
    log_app("LMIC event: %s", ((unsigned int)event < sizeof(evnames)/sizeof(evnames[0])) ? evnames[event] : "EVENT_???" );
}


void cb_lora (sf_lora_callback_args_t * p_args)
{
    _print_event(p_args->event);

    switch (p_args->event)
    {
         case EVENT_LORA_JOINING:
             _isJoined = false;
             break;

         case EVENT_LORA_JOINED:
             _isJoined = true;
             _isDoneTX = true;
             break;

         case EVENT_LORA_JOIN_FAILED:
             _isJoined = false;
             break;

         case EVENT_LORA_TXSTART:
             _isDoneTX = false;
             break;

         case EVENT_LORA_TXDONE:
             break;

         case EVENT_LORA_RXCOMPLETE:
         case EVENT_LORA_TXCOMPLETE:
             if (p_args->rx_data.buff != NULL)
             {
                 char s[256];
                 memcpy(s, p_args->rx_data.buff, p_args->rx_data.buff_size);
                 s[p_args->rx_data.buff_size] = 0;
                 log_app("Down-link data (%d bytes): %s" NL, p_args->rx_data.buff_size, s);
             }
             _isDoneTX = true;
             break;

         default:
             break;
     }
}

