/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2020 Miromico AG
 * All rights reserved.
 */

#include "led.h"

#include "devboard.h"

#include <stdbool.h>

void setRGB(bool red, bool green, bool blue)
{
    g_ioport.p_api->pinWrite(PIN_LED_R, red?   IOPORT_LEVEL_LOW : IOPORT_LEVEL_HIGH);
    g_ioport.p_api->pinWrite(PIN_LED_G, green? IOPORT_LEVEL_LOW : IOPORT_LEVEL_HIGH);
    g_ioport.p_api->pinWrite(PIN_LED_B, blue?  IOPORT_LEVEL_LOW : IOPORT_LEVEL_HIGH);
}
