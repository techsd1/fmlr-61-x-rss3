/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2020 Miromico AG
 * All rights reserved.
 */

#pragma once

#include "hal_data.h"

#include <stdbool.h>

#define PIN_FLASH_CSN   IOPORT_PORT_01_PIN_04
#define PIN_RADIO_DIO1  IOPORT_PORT_00_PIN_15
#define PIN_MODULE_EXTI IOPORT_PORT_00_PIN_02

#define PIN_LED_R       IOPORT_PORT_02_PIN_13
#define PIN_LED_G       IOPORT_PORT_02_PIN_12
#define PIN_LED_B       IOPORT_PORT_00_PIN_14

#define PIN_BUTTON      IOPORT_PORT_00_PIN_01

//#define PIN_I2C_SCL       IOPORT_PORT_04_PIN_00
//#define PIN_I2C_SDA       IOPORT_PORT_04_PIN_07

void setRGB(bool red, bool green, bool blue);
