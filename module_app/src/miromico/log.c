/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2020 Miromico AG
 * All rights reserved.
 */

#include "log.h"

#include "lora_thread.h"
#include "hal_data.h"

#include <stdarg.h>
#include <stdio.h>

void log_app(char const *format, ...) {
    static char buf[256];
    va_list arg;
    va_start(arg, format);
    vsnprintf(buf, sizeof(buf), format, arg);
    va_end(arg);
    // Send via UART
    if (strlen(buf) > 0) {
        g_sf_comms0.p_api->write(g_sf_comms0.p_ctrl, (uint8_t*)buf, strlen(buf), 10);
    }
    g_sf_comms0.p_api->write(g_sf_comms0.p_ctrl, (uint8_t*)NL, strlen(NL), 10);
}

void log_lora (const char* message)
{
    // note that logging while other USART output is ongoing
    // will lead to timeout caused by mutex in SSP framework
    // -> better use a message queue and dedicated logging thread
    log_app("> %s", message);
}

void print_logo() {
    log_app("");
    log_app(" __  __ _____ _____   ____  __  __ _____ _____ ____");
    log_app("|  \\/  |_   _|  __ \\ / __ \\|  \\/  |_   _/ ____/ __ \\");
    log_app("| \\  / | | | | |__) | |  | | \\  / | | || |   | |  | |");
    log_app("| |\\/| | | | |  _  /| |  | | |\\/| | | || |   | |  | |");
    log_app("| |  | |_| |_| | \\ \\| |__| | |  | |_| || |___| |__| |");
    log_app("|_|  |_|_____|_|  \\_\\\\____/|_|  |_|_____\\_____\\____/");
    log_app("");
    log_app("Copyright (c) 2020 Miromico AG");
    log_app("All rights reserved. ");
    log_app("");
}
